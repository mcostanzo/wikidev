// Select the canvas element by its ID
const canvas = document.getElementById('river');
const ctx = canvas.getContext('2d');

// Ensure the canvas matches the document's height and width
function resizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = document.documentElement.scrollHeight;
}

// Draw the line based on the scroll position
function drawLine() {
    // Clear the canvas before drawing
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Line characteristics
    const lineWidth = 60;
    const lineX = canvas.width / 2; // Centered horizontally
    const startY = 400; // Start at 400px to the top
    const scrollY = window.scrollY + window.innerHeight / 2;

    // Get the position of the anchor elements
    const anchor1 = document.getElementById('anchor1');
    const anchor2 = document.getElementById('anchor2');
    const anchor3 = document.getElementById('anchor3');
    const anchor1Top = anchor1.getBoundingClientRect().top + window.scrollY;
    const anchor2Top = anchor2.getBoundingClientRect().top + window.scrollY;
    const anchor3Top = anchor3.getBoundingClientRect().top + window.scrollY;

    // Draw the vertical line
    ctx.beginPath();
    ctx.moveTo(lineX, startY);

    // Set initial line color
    ctx.strokeStyle = '#83060F';

    if (scrollY < anchor1Top) {
        // Draw vertical line until the first anchor is reached
        ctx.lineTo(lineX, scrollY);
    } else if (scrollY < anchor2Top) {
        // Draw vertical line until the first anchor
        ctx.lineTo(lineX, anchor1Top);

        // Draw rounded curve to the left
        const curveRadius = 50;
        ctx.arcTo(lineX, anchor1Top + curveRadius, lineX - curveRadius, anchor1Top + curveRadius, curveRadius);

        // Draw horizontal line to the left for 300px
        const horizontalEndX = lineX - 300;
        ctx.lineTo(horizontalEndX, anchor1Top + curveRadius);

        // Draw rounded curve back to vertical
        ctx.arcTo(horizontalEndX - curveRadius, anchor1Top + curveRadius, horizontalEndX - curveRadius, anchor1Top + 2 * curveRadius, curveRadius);

        // Draw vertical line downwards from the end of the curve
        const verticalStartY = anchor1Top + 2 * curveRadius;
        ctx.lineTo(horizontalEndX - curveRadius, Math.max(verticalStartY, scrollY));
    } else if (scrollY < anchor3Top) {
        // Draw vertical line until the first anchor
        ctx.lineTo(lineX, anchor1Top);

        // Draw rounded curve to the left
        const curveRadius = 50;
        ctx.arcTo(lineX, anchor1Top + curveRadius, lineX - curveRadius, anchor1Top + curveRadius, curveRadius);

        // Draw horizontal line to the left for 300px
        const horizontalEndX = lineX - 300;
        ctx.lineTo(horizontalEndX, anchor1Top + curveRadius);

        // Draw rounded curve back to vertical
        ctx.arcTo(horizontalEndX - curveRadius, anchor1Top + curveRadius, horizontalEndX - curveRadius, anchor1Top + 2 * curveRadius, curveRadius);

        // Draw vertical line downwards from the end of the curve until the second anchor
        const verticalStartY = anchor1Top + 2 * curveRadius;
        ctx.lineTo(horizontalEndX - curveRadius, Math.min(anchor2Top, scrollY));

        if (scrollY >= anchor2Top) {
            // Draw rounded curve to the right at the second anchor
            ctx.arcTo(horizontalEndX - curveRadius, anchor2Top + curveRadius, horizontalEndX + 100 - curveRadius, anchor2Top + curveRadius, curveRadius);

            // Draw horizontal line to the right for 600px
            const secondHorizontalEndX = horizontalEndX + 600;
            const secondHorizontalMidX = horizontalEndX + 300; // Midpoint of the second horizontal segment

            // Create gradient for the second horizontal segment
            const gradient = ctx.createLinearGradient(horizontalEndX, anchor2Top + curveRadius, secondHorizontalEndX, anchor2Top + curveRadius);
            gradient.addColorStop(0, '#83060F');
            gradient.addColorStop(0.5, '#0096FF');

            ctx.strokeStyle = gradient;
            ctx.lineTo(secondHorizontalEndX, anchor2Top + curveRadius);

            // Draw rounded curve back to vertical
            ctx.arcTo(secondHorizontalEndX + curveRadius, anchor2Top + curveRadius, secondHorizontalEndX + curveRadius, anchor2Top + 2 * curveRadius, curveRadius);

            // Draw vertical line downwards from the end of the curve
            const secondVerticalStartY = anchor2Top + 2 * curveRadius;
            ctx.lineTo(secondHorizontalEndX + curveRadius, Math.max(secondVerticalStartY, scrollY));
        }
    } else {
        // Draw vertical line until the first anchor
        ctx.lineTo(lineX, anchor1Top);

        // Draw rounded curve to the left
        const curveRadius = 50;
        ctx.arcTo(lineX, anchor1Top + curveRadius, lineX - curveRadius, anchor1Top + curveRadius, curveRadius);

        // Draw horizontal line to the left for 300px
        const horizontalEndX = lineX - 300;
        ctx.lineTo(horizontalEndX, anchor1Top + curveRadius);

        // Draw rounded curve back to vertical
        ctx.arcTo(horizontalEndX - curveRadius, anchor1Top + curveRadius, horizontalEndX - curveRadius, anchor1Top + 2 * curveRadius, curveRadius);

        // Draw vertical line downwards from the end of the curve until the second anchor
        const verticalStartY = anchor1Top + 2 * curveRadius;
        ctx.lineTo(horizontalEndX - curveRadius, Math.min(anchor2Top, scrollY));

        if (scrollY >= anchor2Top) {
            // Draw rounded curve to the right at the second anchor
            ctx.arcTo(horizontalEndX - curveRadius, anchor2Top + curveRadius, horizontalEndX + 100 - curveRadius, anchor2Top + curveRadius, curveRadius);

            // Draw horizontal line to the right for 600px
            const secondHorizontalEndX = horizontalEndX + 600;
            const secondHorizontalMidX = horizontalEndX + 300; // Midpoint of the second horizontal segment

            // Create gradient for the second horizontal segment
            const gradient = ctx.createLinearGradient(horizontalEndX, anchor2Top + curveRadius, secondHorizontalEndX, anchor2Top + curveRadius);
            gradient.addColorStop(0, '#83060F');
            gradient.addColorStop(0.5, '#0096FF');

            ctx.strokeStyle = gradient;
            ctx.lineTo(secondHorizontalEndX, anchor2Top + curveRadius);

            // Draw rounded curve back to vertical
            ctx.arcTo(secondHorizontalEndX + curveRadius, anchor2Top + curveRadius, secondHorizontalEndX + curveRadius, anchor2Top + 2 * curveRadius, curveRadius);

            // Draw vertical line downwards from the end of the curve until the third anchor
            const secondVerticalStartY = anchor2Top + 2 * curveRadius;
            ctx.lineTo(secondHorizontalEndX + curveRadius, Math.min(anchor3Top, scrollY));
        }
    }

    ctx.lineWidth = lineWidth;
    ctx.stroke();
}

// Update the line drawing as the user scrolls
function onScroll() {
    drawLine();
}

// Initial setup
window.addEventListener('resize', resizeCanvas);
window.addEventListener('scroll', onScroll);

// Set the canvas size and draw the initial line
resizeCanvas();
drawLine();