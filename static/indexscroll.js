document.addEventListener("DOMContentLoaded", function() {
    const sections = document.querySelectorAll('section');
    const navLinks = document.querySelectorAll('.index a');
    const collapsibleMenus = document.querySelectorAll('.is-collapsible');

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            const id = entry.target.getAttribute('id');
            const link = document.querySelector(`.index a[href="#${id}"]`);
            if (entry.isIntersecting) {
                link.classList.add('active');

                // Verifica se l'id del titolo appartiene a un menu collapsible
                collapsibleMenus.forEach(menu => {
                    const ids = menu.id.split(' '); // Gli id corrispondenti ai titoli
                    if (ids.includes(id)) {
                        menu.classList.add('active');
                    }
                });
            } else {
                link.classList.remove('active');

                // Rimuove la classe active dal menu collapsible se nessuna delle sezioni è visibile
                collapsibleMenus.forEach(menu => {
                    const ids = menu.id.split(' '); // Gli id corrispondenti ai titoli
                    if (ids.includes(id)) {
                        // Controlliamo se almeno una sezione associata è ancora visibile
                        let isAnyVisible = false;
                        ids.forEach(id => {
                            const section = document.getElementById(id);
                            if (section && section.getBoundingClientRect().top >= 0 && section.getBoundingClientRect().bottom <= window.innerHeight) {
                                isAnyVisible = true;
                            }
                        });

                        if (!isAnyVisible) {
                            menu.classList.remove('active');
                        }
                    }
                });
            }
        });
    }, { threshold: 0 }); // se i testi saranno tanto lunghi impostare il threshold a 0

    sections.forEach(section => {
        observer.observe(section);
    });
});